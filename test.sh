rails s -p 5000 &
RAILS_PID=$!
sleep 2

FIRST=$(curl -i -s localhost:5000/home/index)
echo "$FIRST"
ETAG_HEADER=$(echo "$FIRST" | grep Etag)
ETAG="${ETAG_HEADER##Etag: }"
IF_NONE_MATCH="If-None-Match: $ETAG"
curl -i -s -H "$IF_NONE_MATCH" localhost:5000/home/index

echo ''
kill $RAILS_PID

